# Projected grid water plane rendering demo #

### This application is designed for demonstration of the projected grid rendering algorithm ###

* [Based on Habib's water shader solution](http://habib.wikidot.com/projected-grid-ocean-shader-full-html-version)
* [Uses pifi engine](https://code.google.com/p/pifi-engine/)

### How do I get set up? ###

* Download pifi: git clone https://code.google.com/p/pifi-engine/
* Clone this repository
* Open ProjectedGrid.dpr using Embarcadero XE3+