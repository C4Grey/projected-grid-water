unit uMain;
(*
  TODO: Projector matrix fix(it has to correct artifacts with stretching of
    vertices on the horizon)
  TODO: Reflections with stencil-based clipping
  TODO: Lighting
  TODO: Try other normal maps
*)

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.ExtCtrls,
  //pifi
  dglOpengl,
  uGLViewer,
  uBaseGL,
  uBaseTypes,
  uVMath,
  uPrimitives,
  uMiscUtils,
  uRenderResource,
  uBaseRenders,
  uGLRenders,
  //project
  uCamera,
  uGridMesh,
  uWater,
  uSkyBox,
  uFrustum,
  uGLtexture;

type
  TForm1 = class(TForm)
    MainViewer: TGLViewer;
    FPSTimer: TTimer;
    WavesTimer: TTimer;
    procedure FPSTimerTimer(Sender: TObject);
    procedure MainViewerCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure MainViewerContextReady(Sender: TObject);
    procedure MainViewerRender(Sender: TObject);
    procedure MainViewerMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure MainViewerMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure MainViewerMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure MainViewerKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure WavesTimerTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    MX, MY: Integer;
    FWater: TWaterObject;
    FSkyBox: TSkyBox;
    FNormal1, FNormal2, FSkyBoxTexture: TGLTexture;
    FTeapod: TGLVertexObject;
    FPlane: TGLVertexObject;
    FFrustum: TFrustumObject;
    FProjectorFrustum: TFrustumObject;
    FCamera: TCamera;
    function InitShader(const AVertex, AFragment:String): TGLSLShaderProgram;
  end;

var
  Form1: TForm1;
  FTime: Single;
  GridShader: TGLSLShaderProgram;
  WaterShader: TGLSLShaderProgram;
  ObjectShader: TGLSLShaderProgram;
  FrustumShader: TGLSLShaderProgram;
  SkyBoxShader: TGLSLShaderProgram;
  ViewDefault, Model, WaterModel: TMatrix;
  vDrawStyle: SmallInt = GL_FILL;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  FCamera := TCamera.Create();
end;

procedure TForm1.FPSTimerTimer(Sender: TObject);
begin
  Form1.Caption := Format('FPS: %f', [MainViewer.FPS]);
end;

procedure TForm1.MainViewerCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  FCamera.SetViewport(NewWidth, NewHeight);
end;

procedure TForm1.MainViewerContextReady(Sender: TObject);
var
  ver: TApiVersion;
  i, j: Integer;
begin
  ver.GAPI := avGL;
  ver.Version := 330;

  GridShader := InitShader('Grid', 'Grid');
  WaterShader := InitShader('Water', 'Water');
  ObjectShader := InitShader('Shader', 'Shader');
  FrustumShader := InitShader('Frustum', 'Frustum');
  SkyBoxShader := InitShader('SkyBox', 'SkyBox');

  FCamera.SetViewport(MainViewer.Width, MainViewer.Height);
  FCamera.Position := TVector.Make(-16, 3, -5);
  ViewDefault := FCamera.View;

  Model.SetIdentity;
  WaterModel.SetIdentity;

  FWater := TWaterObject.Create;
  FWater.Shader := GridShader;
  FWater.Camera := FCamera;

  FFrustum := TFrustumObject.Create;
  FFrustum.Shader := FrustumShader;
  FFrustum.Camera := FCamera;

  FProjectorFrustum := TFrustumObject.Create;
  FProjectorFrustum.SetColor(0.28, 0.6, 0.12);
  FProjectorFrustum.Shader := FrustumShader;
  FProjectorFrustum.Camera := FCamera;

  FTeapod := TGLVertexObject.CreateFrom(CreateTeapod(4));
  FTeapod.Shader := ObjectShader;

  FNormal1 := TGLTexture2D.Create('Textures\gale1.jpg');
  FNormal2 := TGLTexture2D.Create('Textures\gale2.jpg');
  FSkyBoxTexture := TGLTextureCube.Create('Textures\skybox\');

  FSkyBox := TSkyBox.Create(FSkyBoxTexture, SkyBoxShader);
  FSkyBox.Camera := FCamera;
end;

procedure TForm1.MainViewerKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_F1:
      if vDrawStyle = GL_FILL then
        vDrawStyle := GL_LINE
      else
        vDrawStyle := GL_FILL;
  end;
end;

procedure TForm1.MainViewerMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  MX := X;
  MY := Y;
end;

procedure TForm1.MainViewerMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if Shift = [ssLeft] then
    FCamera.RotateAroundTarget(MY - Y, MX - X);
  MX := X;
  MY := Y;
end;

procedure TForm1.MainViewerMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
  FCamera.Move(WheelDelta / 32);
end;

procedure TForm1.MainViewerRender(Sender: TObject);
var
  vModelView: TMatrix;
begin
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

  FSkyBox.Render();

  {$REGION 'Objects'}
  vModelView := Model * FCamera.View;
  ObjectShader.Apply;
  ObjectShader.SetUniform('ModelView', vModelView.Matrix4);
  ObjectShader.SetUniform('ProjMatrix', FCamera.Projection.Matrix4);
  ObjectShader.UnApply;
  FTeapod.RenderVO();
  {$ENDREGION 'Objects'}

  FFrustum.Render(ViewDefault);

  FNormal1.Apply(0);
  FNormal2.Apply(1);
  FSkyBoxTexture.Apply(2);
  glPolygonMode(GL_FRONT_AND_BACK, vDrawStyle);
  FWater.Time := FTime;
//  FWater.Render(FCamera.View);
  FWater.Render(ViewDefault);

  FProjectorFrustum.Render(FWater.Projector);
end;

procedure TForm1.WavesTimerTimer(Sender: TObject);
begin
  FTime := FTime + 0.024;
end;

function TForm1.InitShader(const AVertex, AFragment:String): TGLSLShaderProgram;
const
  path: String = 'Shaders\';
begin
  Result := TGLSLShaderProgram.Create;
  Result.AttachShaderFromFile(stVertex, path + AVertex + '.Vert');
  Result.AttachShaderFromFile(stFragment, path + AFragment + '.Frag');
  Result.LinkShader;
  if Result.Error then begin
    ShowMessage(Result.Log);
    Halt(0);
  end;
end;

end.
