unit uGLTexture;

interface

uses
  dglOpenGL,
  uBaseTypes, Dialogs, SysUtils,
  ImageLoader;

const
  CUBEMAP_FACES_COUNT = 6;

type
  TGLTexture = class
  protected
    FHandle: Cardinal;
    procedure Load(const AFile: String); virtual; abstract;
    procedure Allocate; virtual; abstract;
  public
    constructor Create(const AFile: String);
    procedure Apply(const AUnit:Word = 0); virtual; abstract;
  end;

  TGLTexture2D = class(TGLTexture)
  protected
    FDescription: TImageDesc;
    procedure Allocate; override;
    procedure Load(const AFile: String); override;
  public
    procedure Apply(const AUnit:Word = 0); override;
  end;

  TGLTextureCube = class(TGLTexture)
  protected
    FFaces:Array[0..CUBEMAP_FACES_COUNT - 1] of TImageDesc;
    procedure Load(const AFile: String); override;
    procedure Allocate; override;
  public
    procedure Apply(const AUnit:Word = 0); override;
  end;

implementation

{$REGION 'Texture'}

constructor TGLTexture.Create(const AFile: String);
begin
  Load(AFile);
  Allocate;
end;

{$ENDREGION 'Texture'}

{$REGION 'Texture 2D'}

procedure TGLTexture2D.Load(const AFile: String);
begin
  FDescription := LoadImage(AFile)
end;

procedure TGLTexture2D.Allocate();
begin
  glGenTextures(1, @FHandle);
  glBindTexture(GL_TEXTURE_2D, FHandle);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  with FDescription do
    glTexImage2D(GL_TEXTURE_2D, 0, InternalFormat, Width, Height, 0, ColorFormat, DataType, Data);
  glGenerateMipmap(GL_TEXTURE_2D);
end;

procedure TGLTexture2D.Apply(const AUnit:Word = 0);
begin
  glActiveTexture(GL_TEXTURE0 + AUnit);
  glBindTexture(GL_TEXTURE_2D, FHandle);
end;

{$ENDREGION 'Texture 2D'}

{$REGION 'Texture Cube'}

procedure TGLTextureCube.Load(const AFile: String);
var
  i:Integer;
const
  Faces:Array[0..CUBEMAP_FACES_COUNT - 1] of String =
    ('posx', 'negx', 'posy', 'negy', 'posz', 'negz');
begin
  for i := 0 to CUBEMAP_FACES_COUNT - 1 do
    FFaces[i] := LoadImage(AFile + Faces[i] + '.jpg');
end;

procedure TGLTextureCube.Allocate();
const
  GLFaces:Array[0..CUBEMAP_FACES_COUNT - 1] of Word =
  (
    GL_TEXTURE_CUBE_MAP_POSITIVE_X,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
  );
var
  i:Integer;
begin
  glEnable(GL_TEXTURE_CUBE_MAP);
  glGenTextures(1, @FHandle);
  glBindTexture(GL_TEXTURE_CUBE_MAP, FHandle);
  glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_REPEAT);
  for i := 0 to CUBEMAP_FACES_COUNT - 1 do
  with FFaces[i] do
    glTexImage2D(GLFaces[i], 0, InternalFormat, Width, Height, 0, ColorFormat, DataType, Data);
  glDisable(GL_TEXTURE_CUBE_MAP);
end;

procedure TGLTextureCube.Apply(const AUnit:Word = 0);
begin
  glActiveTexture(GL_TEXTURE0 + AUnit);
  glBindTexture(GL_TEXTURE_CUBE_MAP, FHandle);
end;

{$ENDREGION 'Texture Cube'}

end.
