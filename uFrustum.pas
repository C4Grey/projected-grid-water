unit uFrustum;

interface

uses
  uBaseGL,
  uVMath,
  uMath,
  uLists,
  uBaseTypes,
  uPrimitives,
  uRenderResource,
  uCamera;

type
  TFrustumObject = class
  private
    FObject: TGLVertexObject;
    FShader: TGLSLShaderProgram;
    FModel: TMatrix;
    FCamera: TCamera;
    FColor: TVector;
    function GetData: TVertexObject;
    function GetShader: TGLSLShaderProgram;
    procedure SetShader(const Value: TGLSLShaderProgram);
  public
    constructor Create();
    procedure Render(const ADefalutView: TMatrix);
    procedure SetColor(ARed, AGreen, ABlue: Single);
    property Shader: TGLSLShaderProgram read GetShader write SetShader;
    property Camera: TCamera read FCamera write FCamera;
  end;

implementation

const
  Vertices: Array[0..23] of Vec3 =
  (
    //front
    (-1.0, -1.0, -1.0),
		(+1.0, -1.0, -1.0),
		(-1.0, -1.0, -1.0),
		(-1.0, +1.0, -1.0),
		(+1.0, -1.0, -1.0),
		(+1.0, +1.0, -1.0),
		(-1.0, +1.0, -1.0),
		(+1.0, +1.0, -1.0),
		// back
		(-1.0, -1.0, +1.0),
		(+1.0, -1.0, +1.0),
		(-1.0, -1.0, +1.0),
		(-1.0, +1.0, +1.0),
		(+1.0, -1.0, +1.0),
		(+1.0, +1.0, +1.0),
		(-1.0, +1.0, +1.0),
		(+1.0, +1.0, +1.0),
		// connects
		(-1.0, -1.0, -1.0),
		(-1.0, -1.0, +1.0),
		(+1.0, -1.0, -1.0),
		(+1.0, -1.0, +1.0),
		(-1.0, +1.0, -1.0),
		(-1.0, +1.0, +1.0),
		(+1.0, +1.0, -1.0),
		(+1.0, +1.0, +1.0)
  );
{ TFrustumObject }

constructor TFrustumObject.Create();
begin
  FObject := TGLVertexObject.CreateFrom(GetData);
  FModel.SetIdentity;
  SetColor(1, 1, 1);
  FColor.W := 1;
end;

procedure TFrustumObject.Render(const ADefalutView: TMatrix);
var
  vModelView:TMatrix;
  ViewProjectInverted:TMatrix;
begin
  ViewProjectInverted := ADefalutView * FCamera.Projection;
  ViewProjectInverted.SetInvert;

  vModelView := FModel * FCamera.ViewProjection;

  FShader.Apply;
  FShader.SetUniform('Color', FColor.Vec4);
  FShader.SetUniform('ModelViewProjection', vModelView.Matrix4);
  FShader.SetUniform('ViewProjectionInverted', ViewProjectInverted.Matrix4);
  FShader.UnApply;
  FObject.RenderVO();
end;

function TFrustumObject.GetData: TVertexObject;
var
  vAttribute: TAttribBuffer;
  vVertices: TVec3List;
  i:Integer;
begin
  Result := TVertexObject.Create;
  vVertices := TVec3List.Create;
  for i := 0 to 23 do
    vVertices.Add(Vertices[i]);
  vAttribute:=TAttribBuffer.CreateAndSetup(CAttribSematics[atVertex].Name, 3, vtFloat, 0);
  vAttribute.Buffer.Allocate(vVertices.Size,vVertices.Data);
  vAttribute.Buffer.SetDataHandler(vVertices, True);
  vAttribute.SetAttribSemantic(atVertex);
  Result.AddAttrib(vAttribute, True);
  Result.FaceType := ftLines;
end;

function TFrustumObject.GetShader: TGLSLShaderProgram;
begin
  Result := FShader;
end;

procedure TFrustumObject.SetColor(ARed, AGreen, ABlue: Single);
begin
  FColor.X := ARed;
  FColor.Y := AGreen;
  FColor.Z := ABlue;
end;

procedure TFrustumObject.SetShader(const Value: TGLSLShaderProgram);
begin
  FShader := Value;
  FObject.Shader := FShader;
end;

end.
