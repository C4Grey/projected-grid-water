program ProjectedGrid;

uses
  Vcl.Forms,
  uMain in 'uMain.pas' {Form1},
  uGridMesh in 'uGridMesh.pas',
  uWater in 'uWater.pas',
  uCamera in 'uCamera.pas',
  uFrustum in 'uFrustum.pas',
  uGLTexture in 'uGLTexture.pas',
  uSkyBox in 'uSkyBox.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
