object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Projected grid'
  ClientHeight = 450
  ClientWidth = 607
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object MainViewer: TGLViewer
    Left = 0
    Top = 0
    Width = 607
    Height = 450
    OnRender = MainViewerRender
    OnContextReady = MainViewerContextReady
    Align = alClient
    OnCanResize = MainViewerCanResize
    OnMouseDown = MainViewerMouseDown
    OnMouseMove = MainViewerMouseMove
    OnMouseWheel = MainViewerMouseWheel
    OnKeyDown = MainViewerKeyDown
  end
  object FPSTimer: TTimer
    Interval = 100
    OnTimer = FPSTimerTimer
    Left = 24
    Top = 16
  end
  object WavesTimer: TTimer
    Interval = 16
    OnTimer = WavesTimerTimer
    Left = 128
    Top = 16
  end
end
