unit uGridMesh;

interface

uses
  uVMath,
  uMath,
  uLists,
  uBaseTypes,
  uRenderResource;

function CreateGridMesh(): TVertexObject;
function PlaneMake(const APoint, ANormal:TVector): TVector;
function Evaluate(const AFirst, ASecond:TVector):Single;
function PlaneEvaluatePoint(const APlane, APoint:TVector):Single;
function IntersectLinePlane(const APoint, ADirection, APlane: TVector; var AIntersectPoint: TVector): Integer;
procedure CombineVector(var vr: TVector; const v:TVector; var f: Single);

implementation

function IntersectLinePlane(const APoint, ADirection, APlane: TVector; var AIntersectPoint: TVector): Integer;
var
   a, b : Extended;
   t : Single;
begin
  a:=APlane.Dot(ADirection);
  b:=PlaneEvaluatePoint(APlane, APoint);
  if a=0 then
  begin                  // ADirection is parallel to APlane
    if b=0 then
      Result:=-1        // line is inside APlane
    else
      Result:=0;        // line is outside APlane
  end
  else
  begin
    t:=-b/a;
    AIntersectPoint := APoint;
    CombineVector(AIntersectPoint, ADirection, t);
    Result:=1;
  end;
end;

procedure CombineVector(var vr: TVector; const v:TVector; var f: Single);
begin
   vr.X:=vr.X+v.X*f;
   vr.Y:=vr.Y+v.Y*f;
   vr.Z:=vr.Z+v.Z*f;
end;

function PlaneEvaluatePoint(const APlane, APoint:TVector):Single;
begin
   Result := APlane.X * APoint.X + APlane.Y * APoint.Y + APlane.Z * APoint.Z + APlane.W;
end;

function Evaluate(const AFirst, ASecond:TVector):Single;
begin
   Result := AFirst.X * ASecond.X + AFirst.Y * ASecond.Y + AFirst.Z * ASecond.Z + AFirst.W;
end;

function PlaneMake(const APoint, ANormal:TVector): TVector;
begin
   Result := ANormal;
   Result.W := -APoint.Dot(ANormal);
end;

function CreateGridMesh(): TVertexObject;
var vAttribute: TAttribBuffer;
    vVertices: TVec3List;
    vTexCoords: TVec2List;
    vProjectedSize: Float;
    x, y, vIndex: Integer;
    vX, vY: Float;
const
  vSize:Integer = 32;
begin
  Result:=TVertexObject.Create;
  vVertices:=TVec3List.Create;
  vTexCoords:=TVec2List.Create;
  vProjectedSize := 1 / (vSize);
  for y := 0 to vSize do
  begin
    for x := 0 to vSize do
    begin
      //x = [0..1]; y = [0..1];
      vX := x * vProjectedSize;
      vY := y * vProjectedSize;
      vVertices.Add(Vec3Make(vX, vY));
      vTexCoords.Add(Vec2Make(vX, vY));
    end;
  end;
  vAttribute:=TAttribBuffer.CreateAndSetup(CAttribSematics[atVertex].Name, 3, vtFloat, 0);
  vAttribute.Buffer.Allocate(vVertices.Size,vVertices.Data);
  vAttribute.Buffer.SetDataHandler(vVertices, True);
  vAttribute.SetAttribSemantic(atVertex);
  Result.AddAttrib(vAttribute, True);
  vAttribute:=TAttribBuffer.CreateAndSetup(CAttribSematics[atTexCoord0].Name, 2, vtFloat, 0);
  vAttribute.Buffer.Allocate(vTexCoords.Size,vTexCoords.Data);
  vAttribute.Buffer.SetDataHandler(vTexCoords, True);
  vAttribute.SetAttribSemantic(atTexCoord0);
  Result.AddAttrib(vAttribute);
  for y := 0 to vSize - 1 do
  begin
    for x := 0 to vSize - 1 do
    begin
      vIndex := (vSize + 1) * y + x;
      Result.AddPoint(vIndex);
      Result.AddPoint(vIndex + 1);
      Result.AddPoint(vIndex + vSize + 1);
      Result.AddPoint(vIndex + vSize + 1);
      Result.AddPoint(vIndex + 1);
      Result.AddPoint(vIndex + vSize + 2);
    end;
  end;
  Result.FaceType:=ftTriangles;
end;

end.
