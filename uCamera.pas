unit uCamera;

interface

uses
  uVMath;

type
  TCamera = class
  private
    FPosition: TVector;
    FDirection: TVector;
    FTarget: TVector;
    FUp: TVector;
    FProjection: TMatrix;
    FView: TMatrix;
    FViewProjection: TMatrix;
    FNear: Single;
    FFar: Single;
    FFOV: Single;
    function GetPosition: TVector;
    function GetDirection: TVector;
    procedure SetPosition(const Value: TVector);
    procedure Update();
    function GetTarget: TVector;
    procedure SetTarget(const Value: TVector);
  public
    constructor Create();
    procedure SetViewport(AWidth, AHeight: Integer);
    procedure RotateAroundTarget(const APitch, ATurn: Single);
    procedure Move(ADelta: Single);
    property Position: TVector read GetPosition write SetPosition;
    property Taget: TVector read GetTarget write SetTarget;
    property Direction: TVector read GetDirection;
    property View: TMatrix read FView;
    property ViewProjection: TMatrix read FViewProjection;
    property Projection: TMatrix read FProjection;
    property ZFar: Single read FFar;
    property ZNear: Single read FNear;
  end;

implementation

{ TCamera }

constructor TCamera.Create();
begin
  FTarget := VecNull;
  FPosition := VecNull;
  FDirection := VecNull;
  FUp := VecY;
  FNear := 0.1;
  FFar := 1024;
  FFOV := 60;
end;

function TCamera.GetDirection: TVector;
begin
  Result := FDirection;
end;

function TCamera.GetPosition: TVector;
begin
  Result := FPosition;
end;

function TCamera.GetTarget: TVector;
begin
  Result := FTarget;
end;

procedure TCamera.Move(ADelta: Single);
begin
  FPosition := FPosition + FDirection * ADelta;
  Update();
end;

procedure TCamera.RotateAroundTarget(const APitch, ATurn: Single);
begin
  FPosition.RotateAround(FTarget, FUp, APitch, ATurn);
  Update();
end;

procedure TCamera.SetPosition(const Value: TVector);
begin
  FPosition := Value;
  Update();
end;

procedure TCamera.SetTarget(const Value: TVector);
begin
  FTarget := Value;
  Update();
end;

procedure TCamera.SetViewport(AWidth, AHeight: Integer);
begin
  FProjection := TMatrix.PerspectiveMatrix(FFOV, AWidth / AHeight, FNear, FFar);
  FViewProjection := FView * FProjection;
end;

procedure TCamera.Update();
begin
  FView := TMatrix.LookAtMatrix(FPosition, FTarget, FUp);
  FViewProjection := FView * FProjection;
  FDirection.X := FView.A[0, 2];
  FDirection.Y := FView.A[1, 2];
  FDirection.Z := FView.A[2, 2];
  FDirection.W := 1;
end;

end.
