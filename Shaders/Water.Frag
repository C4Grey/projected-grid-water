#version 330

uniform sampler2D NormalTexture_0;

in vec2 TexCoord;
in vec3 Normal;
in vec3 View;

layout(location = 0) out vec4 FragColor;

void main() 
{
	vec3 Normal_0 = texture2D(NormalTexture_0, TexCoord).xyz;
    Normal_0 = Normal_0 * vec3(2.0, 2.0, 2.0) - vec3(1.0, 1.0, 1.0);
    Normal_0 = normalize(Normal_0 + Normal);

	float Fresnel = 1.0 - dot(normalize(View), Normal_0);
	
	FragColor.xyz = vec3(Fresnel, Fresnel, Fresnel);
	FragColor.a = 1.0;
}