unit uSkyBox;

interface

uses
  dGLOpenGL,
  uPrimitives,
  uBaseGL,
  uVMath,
  uGLTexture,
  uCamera;

type
  TSkyBox = class
  private
    FObject: TGLVertexObject;
    FTexture: TGLTexture;
    FShader: TGLSLShaderProgram;
    FModel:TMatrix;
    FCamera: TCamera;
  public
    constructor Create(ATexture: TGLTexture; AShader: TGLSLShaderProgram);
    procedure Render();
    property Camera: TCamera read FCamera write FCamera;
  end;

implementation

{ TSkyBox }

constructor TSkyBox.Create(ATexture: TGLTexture; AShader: TGLSLShaderProgram);
begin
  FTexture := ATexture;
  FShader := AShader;
  FObject := TGLVertexObject.CreateFrom(CreateCube(1));
  FObject.Shader := FShader;
  FModel.SetIdentity;
end;

procedure TSkyBox.Render();
var
  vMVP: TMatrix;
  vMVPInverse: TMatrix;
begin
  glDepthMask(False);

  FModel := TMatrix.TranslationMatrix(FCamera.Position);

  vMVP := FModel * FCamera.ViewProjection;
  FTexture.Apply(0);
  FShader.Apply;
  FShader.SetUniform('uTexture', 0);
  FShader.SetUniform('ModelViewProjection', vMVP.Matrix4);
  FShader.UnApply;
  FObject.RenderVO();

  glDepthMask(True);
end;

end.
