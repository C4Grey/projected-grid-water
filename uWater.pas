unit uWater;

interface

uses
  dglOpenGL,
  uBaseGL,
  uBaseTypes,
  uVMath,
  uPrimitives,
  uMiscUtils,
  uRenderResource,
  uBaseRenders,
  uGLRenders,
  uLists,
  uGridMesh,
  uCamera;

type
  TWaterObject = class
  private
    FPosition: TVector;
    FNormal: TVector;
    FPlane: TVector;
    FTop: TVector;
    FBottom: TVector;
    FGrid: TGLVertexObject;
    FShader: TGLSLShaderProgram;
    FView: TMatrix;
    FProjectorView: TMatrix;
    FModel: TMatrix;
    FTBNMatrix: TMatrix;
    FProjection: TMatrix;
    FProjectViewInverted: TMatrix;
    FCameraPosition: TVector;
    FCorrectedProjector: TMatrix;
    FTime:Single;
    FCamera: TCamera;
    function GetShader: TGLSLShaderProgram;
    procedure SetShader(const Value: TGLSLShaderProgram);
    function GetBound: TMatrix;
    procedure ProjectorCorrection;
    function GetProjector: TMatrix;
  public
    constructor Create();
    procedure Render(const AProjectorView: TMatrix);
    property Shader: TGLSLShaderProgram read GetShader write SetShader;
    property Time: Single read FTime write FTime;
    property Projector: TMatrix read GetProjector;
    property Camera: TCamera read FCamera write FCamera;
  end;

implementation

const
  FrustmPointsIDs:Array[0..23] of Byte =
  (
    0,1,	0,2,	2,3,	1,3,
		0,4,	2,6,	3,7,	1,5,
		4,6,	4,5,	5,7,	6,7
	);

constructor TWaterObject.Create;
begin
  FPosition := TVector.Make(0, 0, 0);
  FNormal := TVector.Make(0, 1, 0);
  FPlane := PlaneMake(FPosition, FNormal);
  FTop := FPlane;
  FBottom := FPlane;
  FGrid := TGLVertexObject.CreateFrom(CreateGridMesh());
  FModel.SetIdentity;
  FCorrectedProjector.SetIdentity;

  FModel.A[2, 0] := 0;
  FModel.A[2, 1] := 1;
  FModel.A[2, 2] := 0;

  FModel.A[1, 0] := 0;
  FModel.A[1, 1] := 0;
  FModel.A[1, 2] := 1;

  FTBNMatrix.Row[0] :=  TVector.Make(1, 0, 0, 0).Vec4;
  FTBNMatrix.Row[1] :=  TVector.Make(0, 0, 1, 0).Vec4;
  FTBNMatrix.Row[2] :=  TVector.Make(0, 1, 0, 0).Vec4;
  FTBNMatrix.Row[3] :=  TVector.Make(0, 0, 0, 1).Vec4;
  FTBNMatrix.SetInvert;
end;

procedure TWaterObject.Render(const AProjectorView: TMatrix);
var
  ModelViewProjectionMatrix: TMatrix;
  vNormal: TMatrix;
  vBounds: TMatrix;
  vTransforms: TMatrix;
begin
  vTransforms.Row[0] := TVector.Make(0.008, 0.008, FTime * 0.03 * sin(0), FTime * 0.03 * cos(0)).Vec4;
  vTransforms.Row[1] := TVector.Make(0.04, 0.04, FTime * 0.02 * sin(3.14159), FTime * 0.02 * cos(3.14159)).Vec4;
  vTransforms.Row[2] := TVector.Make(0.016, 0.016, FTime * 0.04 * sin(1.5708), FTime * 0.04 * cos(1.5708)).Vec4;
  vTransforms.Row[3] := TVector.Make(0.1, 0.1, FTime * 0.03 * sin(-1.5708), FTime * 0.03 * cos(-1.5708)).Vec4;

  FView := FCamera.View;
  FProjection := FCamera.Projection;
  FCameraPosition := FCamera.Position;

  FProjectorView := AProjectorView;

  ModelViewProjectionMatrix := FModel * FCamera.ViewProjection;

  vBounds := GetBound;

  FShader.Apply;
  FShader.SetUniform('NormalTexture_0', 0);
  FShader.SetUniform('NormalTexture_1', 1);
  FShader.SetUniform('ReflectionsCubeMap', 2);
  FShader.SetUniform('normal_0_transform', vTransforms.Row[0]);
  FShader.SetUniform('normal_1_transform', vTransforms.Row[1]);
  FShader.SetUniform('normal_2_transform', vTransforms.Row[2]);
  FShader.SetUniform('normal_3_transform', vTransforms.Row[3]);
  FShader.SetUniform('CameraPosition', FCameraPosition.Vec4);
  FShader.SetUniform('Corners', vBounds.Matrix4);
  FShader.SetUniform('ModelMatrix', FModel.Matrix4);
  FShader.SetUniform('ModelViewProjectionMatrix', ModelViewProjectionMatrix.Matrix4);
  FShader.UnApply;

  FGrid.RenderVO();
end;

procedure TWaterObject.ProjectorCorrection;
var
  vDirection: TVector;
  vTarget, vTarget2: TVector;
  vFlipped: TVector;
  vPosition: TVector;
  vFadeFactor: Single;
  function Intersection(const APosition, ADirection: TVector): TVector;
  var
    vDelta: TVector;
    ZeroLvl, K:Single;
    vStart: TVector;
    vFinish: TVector;
  begin
    ZeroLvl := 0;
    vStart := APosition;
    vFinish := APosition + ADirection;
    vDelta := vFinish - vStart;
    K := (vStart.W * ZeroLvl - vStart.Z) / (vDelta.Z - vDelta.W * ZeroLvl);
    Result := vStart + vDelta * K;
  end;

begin
  vDirection.X := FProjectorView.A[0, 2];
  vDirection.Y := FProjectorView.A[1, 2];
  vDirection.Z := FProjectorView.A[2, 2];
  vDirection.W := 1;
  vPosition := TVector.Make(-16, 3, -5);
  vFlipped := vDirection - 2 * FNormal * vDirection.Dot(FNormal);
  IntersectLinePlane(vPosition, vPosition + vFlipped, FPlane, vTarget);
  vFadeFactor := abs(FPlane.Dot(vDirection));

  vTarget2 := vPosition + vDirection * 10;
//  vTarget2 := vTarget2 - FNormal * vTarget2.Dot(FNormal);
  vTarget := vTarget * vFadeFactor + vTarget2 * (1 - vFadeFactor);

  FCorrectedProjector := FProjectorView;//TMatrix.LookAtMatrix(vPosition, vTarget, VecY);
end;



function TWaterObject.GetBound(): TMatrix;
var
  vMatrix: TMatrix;
  vDelta: TVector;
  i: Integer;
  TM: Array [0..7] of TVector;
  ZeroLvl, K:Single;
  vTemp: Tvector;
const
  Frustum: Array [0..7] of Vec4 =
  (
    (-1.0, -1.0, -1.0, 1.0),
    (-1.0, -1.0, +1.0, 1.0),
    (+1.0, -1.0, -1.0, 1.0),
    (+1.0, -1.0, +1.0, 1.0),
    (+1.0, +1.0, -1.0, 1.0),
    (+1.0, +1.0, +1.0, 1.0),
    (-1.0, +1.0, -1.0, 1.0),
    (-1.0, +1.0, +1.0, 1.0)
  );

begin
  ZeroLvl := 0;

  ProjectorCorrection;

  vMatrix := FModel * FCorrectedProjector * FProjection;
  vMatrix := vMatrix.Invert;

  for i := 0 to 7 do
    TM[i] := vMatrix.Transform(Frustum[i]);

  for i := 0 to 3 do
  begin
    vDelta := TM[2*i+1] - TM[2*i];
    K := (TM[2*i].W * ZeroLvl - TM[2*i].Z) / (vDelta.Z - vDelta.W * ZeroLvl);
    vTemp := TM[2*i] + vDelta * K;
    Result.Row[i] := vTemp.Vec4;
  end;
end;

function TWaterObject.GetProjector: TMatrix;
begin
  Result := FCorrectedProjector;
end;

function TWaterObject.GetShader: TGLSLShaderProgram;
begin
  Result := FShader;
end;

procedure TWaterObject.SetShader(const Value: TGLSLShaderProgram);
begin
  FShader := Value;
  FGrid.Shader := FShader;
end;

end.
